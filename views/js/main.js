function convertTOCelcius(val){
    if(typeof val != "number"){
        throw Error("Input should be number")
    }
    if(isNaN(val)){
        throw Error("Input should be number")
    }
    return val * 9 / 5 + 32;
}
function convert(degree) {
    
    var x,y,z,a,b;
    if (degree == "C") {    
        x = convertTOCelcius(document.getElementById("c"));
        y = document.getElementById("c").value+273.15;
        z = (document.getElementById("c").value+273.15) * 9/5;
        a = (100-document.getElementById("c").value)*(3/2);
        b = document.getElementById("c").value * (33/100);
        document.getElementById("f").value = Math.round(x);
        document.getElementById("k").value = Math.round(y);
        document.getElementById("r").value = Math.round(z);
        document.getElementById("d").value = Math.round(a);
        document.getElementById("n").value = Math.round(b);
    } else if(degree == "F"){
        x = (document.getElementById("f").value -32) * 5 / 9;
        y = document.getElementById("f").value+459.67*(5/9);
        z = document.getElementById("f").value+459.67;
        a = 212-document.getElementById("f").value*(5/6);
        b = document.getElementById("f").value-32*(11/60);
        document.getElementById("c").value = Math.round(x);
        document.getElementById("k").value = Math.round(y);
        document.getElementById("r").value = Math.round(z);
        document.getElementById("d").value = Math.round(a);
        document.getElementById("n").value = Math.round(b);
    }
    else if(degree == "K") {
        x = document.getElementById("k").value -273.15 ;
        y = (document.getElementById("k").value * 9/5 )-459.67;
        z = document.getElementById("k").value * 9/5;
        a = (373.15-100-document.getElementById("k").value)*(3/2);
        b = (document.getElementById("k").value - 273.15) * (33/100);
        document.getElementById("c").value = Math.round(x);
        document.getElementById("f").value = Math.round(y);
        document.getElementById("r").value = Math.round(z);
        document.getElementById("d").value = Math.round(a);
        document.getElementById("n").value = Math.round(b);
    }
    else if(degree == "R"){
        x = (document.getElementById("r").value -491.67) * 5/9;
        y = document.getElementById("r").value+459.67;
        z = document.getElementById("r").value * 5/9;
        a = 671.67-document.getElementById("r").value*(5/6);
        b = (document.getElementById("r").value-491.67)* 11/60;
        document.getElementById("c").value = Math.round(x);
        document.getElementById("f").value = Math.round(y);
        document.getElementById("k").value = Math.round(z);
        document.getElementById("d").value = Math.round(a);
        document.getElementById("n").value = Math.round(b);
    }else if(degree == "D"){
        x = (100-document.getElementById("d").value)* 2/3;
        y = (212-document.getElementById("d").value)* 6/5;
        z = (373.15-document.getElementById("d").value) *2/3;
        a = (671.67-document.getElementById("d").value)*(6/5);
        b = (33-document.getElementById("d").value) * (11/50);
        document.getElementById("c").value = Math.round(x);
        document.getElementById("f").value = Math.round(y);
        document.getElementById("k").value = Math.round(z);
        document.getElementById("r").value = Math.round(a);
        document.getElementById("n").value = Math.round(b);
    }else {
        x = document.getElementById("n").value * 100/33;
        y = (document.getElementById("n").value* 60/11)+32;
        z = (document.getElementById("n").value* 100/33) + 273.15;
        a = (document.getElementById("n").value* 60/11) + 491.67;
        b = (33-document.getElementById("n").value )* (50/11);
        document.getElementById("c").value = Math.round(x);
        document.getElementById("f").value = Math.round(y);
        document.getElementById("k").value = Math.round(z);
        document.getElementById("r").value = Math.round(a);
        document.getElementById("d").value = Math.round(b);
    }
}