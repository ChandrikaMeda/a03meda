QUnit.test('Testing the temperature convertTOCelciuser with four sets of inputs', function (assert) {
    // assert.equal(convertTOCelcius(30), 86,303,545,105,9, "works with positive integers");
    // assert.equal(convertTOCelcius(-10), 14,263,473,165,-3, "works with a negative number");
    // assert.equal(convertTOCelcius(0), 32,273,491,150,0, "works with zeroes");
    assert.equal(convertTOCelcius(86),186.8, "works with positive integers");
});

QUnit.test('Testing the temperature convertTOCelciuser with four sets of inputs to check exception handling', function(assert) {
    assert.throws(function() { convertTOCelcius(NaN) }, 'NaN is restricted');
    assert.throws(function() { convertTOCelcius(null) }, 'Null is restricted');
    assert.throws(function() { convertTOCelcius("abc") }, 'String is restricted');
    assert.throws(function() { convertTOCelcius(undefined) }, 'Undefined values are restricted');
});